<?php

namespace Services\FileReaderService;

use Interfaces\IFileReader;

class FileReaderService
{
    const END_MARKER = '__KONIEC__';
    /**
     * @var IFileReader
     */
    private $fileReader;

    /**
     * FileReaderService constructor.
     */
    public function __construct(IFileReader $fileReader)
    {
        $this->fileReader = $fileReader;
    }

    /**
     * @param string $pathFile
     */
    public function readFile(string $pathFile)
    {
        foreach ($this->fileReader->readFile($pathFile) as $line)
        {
            if (false !== strpos($line, self::END_MARKER)) {
                break;
            }
            echo '<p>'.$line.'</p>';
        }
    }
}