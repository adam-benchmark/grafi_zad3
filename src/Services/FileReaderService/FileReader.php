<?php

namespace Services\FileReaderService;

use Interfaces\IFileReader;

class FileReader implements IFileReader
{
    /**
     * @param string $pathFile
     * @return \Generator
     */
    public function readFile(string $pathFile)
    {
        $handle = fopen($pathFile, "r");

        while (!feof($handle)) {
            yield trim(fgets($handle));
        }

        fclose($handle);
    }
}