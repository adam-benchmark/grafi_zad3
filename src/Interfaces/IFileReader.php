<?php

namespace Interfaces;

interface IFileReader
{
    public function readFile(string $pathFile);
}