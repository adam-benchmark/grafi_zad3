##task 3

Napisz funkcję, która w parametrze dostaje ścieżkę do pliku tekstowego a wynik funkcji będzie użyty w pętli do odczytywania kolejnych linijek tekstu z pliku, aż do znalezienia wiersza o treści `__KONIEC__`. Pamiętaj o tym, że plik może mieć bardzo duże rozmiary oraz może wystąpić problem z odczytem pliku.

##quick start
- ściągnij repo
- composer install
- composer dump
- rozruch: /web/index.php